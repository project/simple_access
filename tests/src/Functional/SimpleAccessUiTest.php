<?php

namespace Drupal\Tests\simple_access\Functional;

use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\simple_access\Entity\SimpleAccessGroup;
use Drupal\Tests\BrowserTestBase;

/**
 * Basic tests.
 *
 * @group simple_access
 */
class SimpleAccessUiTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'simple_access',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the UI.
   */
  public function testSimpleAccessUi(): void {
    $type = $this->drupalCreateContentType();
    $admin = $this->drupalCreateUser([
      'manage simple access',
      'assign groups to nodes',
      sprintf('create %s content', $type->id()),
    ]);

    $groupRid = $this->drupalCreateRole([]);
    $group = SimpleAccessGroup::create([
      'id' => 'test_group',
      'label' => 'Test group',
      'roles' => [$groupRid],
    ]);
    $group->save();

    $editor = $this->drupalCreateUser([
      'assign groups to nodes',
      sprintf('create %s content', $type->id()),
    ]);
    $editor->addRole($groupRid)->save();

    $this->drupalLogin($admin);
    // Enable update access.
    $this->drupalGet(Url::fromRoute('simple_access.admin'));
    $assert = $this->assertSession();
    $assert->checkboxChecked('display[view]');
    $assert->checkboxNotChecked('display[update]');
    $assert->checkboxNotChecked('display[delete]');
    $this->submitForm([
      'display[view]' => 'view',
      'display[update]' => 'update',
    ], 'Save configuration');
    $assert->pageTextContains('The configuration options have been saved.');
    $assert->checkboxChecked('display[view]');
    $assert->checkboxChecked('display[update]');
    $assert->checkboxNotChecked('display[delete]');

    // Check node form, group shouldn't be visible based on admin user not
    // being part of role and show_groups config being off.
    $this->drupalGet('node/add/' . $type->id());
    $simpleAccessSettings = $assert->elementExists('css', '.simple-access-settings');
    $tableHeaderText = $assert->elementExists('css', 'table thead', $simpleAccessSettings)->getText();
    // Only shows access that is configured.
    $this->assertStringContainsString('View', $tableHeaderText);
    $this->assertStringContainsString('Update', $tableHeaderText);
    $this->assertStringNotContainsString('Delete', $tableHeaderText);
    $assert->elementNotExists('css', 'input', $simpleAccessSettings);

    // Enable show groups.
    $this->drupalGet(Url::fromRoute('simple_access.admin'));
    $this->submitForm([
      'show_groups' => '1',
    ], 'Save configuration');

    $this->drupalGet('node/add/' . $type->id());
    $assert->elementExists('css', 'input', $simpleAccessSettings);
    $label = $this->randomMachineName();
    $this->submitForm([
      'title[0][value]' => $label,
      'simple_access[groups][test_group][update]' => '1',
    ], 'Save');

    // Current user can't edit the node (no groups or owner permissions).
    $node = Node::load(1);
    $this->assertEquals($label, $node->getTitle());
    $this->drupalGet($node->toUrl('edit-form'));
    $assert->statusCodeEquals(403);

    // Turn off show_groups to test groups are shown for user in the group.
    \Drupal::configFactory()->getEditable('simple_access.settings')->set('show_groups', FALSE)->save();

    // Login as editor with group, they should have edit access.
    $this->drupalLogin($editor);
    $this->drupalGet($node->toUrl('edit-form'));
    $assert->statusCodeEquals(200);
    // Access checkboxes should show as user is in group.
    $assert->elementExists('css', 'input', $simpleAccessSettings);
    // We should be able to empty out access options.
    $this->submitForm([
      'simple_access[groups][test_group][update]' => '0',
    ], 'Save');
    $this->drupalGet($node->toUrl('edit-form'));
    // User removed their own group, access is now denied.
    $assert->statusCodeEquals(403);
  }

}
