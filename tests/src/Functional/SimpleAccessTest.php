<?php

namespace Drupal\Tests\simple_access\Functional;

use Drupal\simple_access\Entity\SimpleAccessGroup;
use Drupal\Tests\BrowserTestBase;

/**
 * Basic tests.
 *
 * @group simple_access
 */
class SimpleAccessTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'simple_access',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests basic simple_access functionality.
   */
  public function testSimpleAccess(): void {
    $type = $this->drupalCreateContentType();

    $rid = $this->drupalCreateRole([]);
    $group = SimpleAccessGroup::create([
      'id' => 'test_group',
      'label' => 'Test group',
      'roles' => [$rid],
    ]);
    $group->save();
    $account = $this->drupalCreateUser();
    $account2 = $this->drupalCreateUser();
    $node1 = $this->drupalCreateNode([
      'type' => $type->id(),
      'uid' => $account->id(),
      'status' => 0,
    ]);
    $node1->simple_access = ['groups' => ['owner' => ['view' => 1]]];
    $node1->save();
    $node2 = $this->drupalCreateNode([
      'type' => $type->id(),
      'uid' => $account->id(),
      'status' => 0,
    ]);
    $node2->simple_access = ['groups' => ['owner' => ['view' => 1, 'update' => 1]]];
    $node2->save();

    // Only owner can view but not edit node1.
    $this->assertTrue($node1->access('view', $account));
    $this->assertFalse($node1->access('update', $account));
    $this->assertFalse($node1->access('delete', $account));
    $this->assertFalse($node1->access('view', $account2));
    $this->assertFalse($node1->access('update', $account2));
    $this->assertFalse($node1->access('delete', $account2));

    // Owner can view and edit node2.
    $this->assertTrue($node2->access('view', $account));
    $this->assertTrue($node2->access('update', $account));
    $this->assertFalse($node2->access('delete', $account));
    $this->assertFalse($node2->access('view', $account2));
    $this->assertFalse($node2->access('update', $account2));
    $this->assertFalse($node2->access('delete', $account2));

    $account->addRole($rid)->save();
    $node1->simple_access = ['groups' => [$group->id() => ['view' => 1]]];
    $node1->save();
    $node2->simple_access = [
      'groups' => [$group->id() => ['view' => 0, 'update' => 1, 'delete' => 1]],
    ];
    $node2->save();
    \Drupal::entityTypeManager()->getAccessControlHandler('node')->resetCache();

    // Group can view node 1 only.
    $this->assertTrue($node1->access('view', $account));
    $this->assertFalse($node1->access('update', $account));
    $this->assertFalse($node1->access('delete', $account));
    $this->assertFalse($node1->access('view', $account2));
    $this->assertFalse($node1->access('update', $account2));
    $this->assertFalse($node1->access('delete', $account2));

    // Group can edit and delete node 2.
    $this->assertFalse($node2->access('view', $account));
    $this->assertTrue($node2->access('update', $account));
    $this->assertTrue($node2->access('delete', $account));
    $this->assertFalse($node2->access('view', $account2));
    $this->assertFalse($node2->access('update', $account2));
    $this->assertFalse($node2->access('delete', $account2));
  }

}
